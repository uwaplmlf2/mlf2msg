/*
Copyright © 2019 Michael Kenney <mikek@apl.uw.edu>

*/
package main

import "bitbucket.org/uwaplmlf2/mlf2msg/cmd"

var Version = "dev"
var BuildDate = "unknown"

func main() {
	cmd.Execute(Version)
}
