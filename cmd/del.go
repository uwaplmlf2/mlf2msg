/*
Copyright © 2019 Michael Kenney <mikek@apl.uw.edu>

*/
package cmd

import (
	"fmt"
	"strconv"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

// delCmd represents the del command
var delCmd = &cobra.Command{
	Use:          "del floatid id",
	Short:        "Remove one or more commands from the queue",
	Args:         cobra.ExactArgs(2),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		floatid, err := strconv.Atoi(args[0])
		if err != nil {
			errors.Wrap(err, "read float ID")
		}
		ch, err := mqSetup(floatid)
		if err != nil {
			return err
		}
		defer ch.Close()

		n, err := delMatch(ch, floatid, args[1])
		if err != nil {
			return err
		}
		fmt.Printf("%d commands removed\n", n)

		return nil
	},
}

func init() {
	rootCmd.AddCommand(delCmd)
}
