package cmd

import (
	"fmt"
	"time"

	"github.com/pkg/errors"
	"github.com/rs/xid"
	"github.com/streadway/amqp"
)

type Summary struct {
	T                time.Time
	Sender, Comment  string
	CommandId, MsgId string
	Command          string
}

func NewSummary(d amqp.Delivery) Summary {
	var s Summary
	s.T = d.Timestamp
	s.Sender = d.ReplyTo
	s.MsgId = d.CorrelationId
	s.Command = string(d.Body)

	if val, ok := d.Headers["comment"]; ok {
		s.Comment, _ = val.(string)
	}

	if val, ok := d.Headers["uuid"]; ok {
		s.CommandId, _ = val.(string)
	}

	// If the command was sent via the web-interface, the database-id
	// serves as the message-id
	if val, ok := d.Headers["dbid"]; ok {
		s.MsgId, _ = val.(string)
	}

	return s
}

func publishCommands(ch *amqp.Channel, exchange string, floatid int, cmds []string,
	sender, comment string) (string, error) {
	headers := make(amqp.Table)
	headers["floatid"] = int32(floatid)
	headers["comment"] = comment

	id := xid.New()
	key := fmt.Sprintf("float-%d.commands", floatid)
	n := len(cmds)
	for i, cmd := range cmds {
		if i == (n - 1) {
			headers["eot"] = "yes"
		}
		headers["uuid"] = xid.New().String()
		msg := amqp.Publishing{
			Headers:       headers,
			ContentType:   "text/plain",
			DeliveryMode:  2,
			CorrelationId: id.String(),
			ReplyTo:       sender,
			Timestamp:     time.Now(),
			Body:          []byte(cmd),
		}

		if err := ch.Publish(exchange, key, false, false, msg); err != nil {
			return "", errors.Wrap(err, "publishCommands")
		}
	}

	return id.String(), nil
}

func getAll(ch *amqp.Channel, floatid int) ([]Summary, error) {
	qname := fmt.Sprintf("float-%d.commands", floatid)
	listing := make([]Summary, 0)
	msgs := make([]amqp.Delivery, 0)

	qinfo, err := ch.QueueInspect(qname)
	if err != nil {
		return listing, err
	}

	for i := 0; i < qinfo.Messages; i++ {
		msg, ok, err := ch.Get(qname, false)
		if !ok || err != nil {
			return listing, err
		}
		listing = append(listing, NewSummary(msg))
		msgs = append(msgs, msg)
	}

	// Requeue messages
	for _, msg := range msgs {
		msg.Reject(true)
	}

	return listing, err
}

func delMatch(ch *amqp.Channel, floatid int, id string) (int, error) {
	qname := fmt.Sprintf("float-%d.commands", floatid)
	qinfo, err := ch.QueueInspect(qname)
	if err != nil {
		return 0, err
	}

	msgs := make([]amqp.Delivery, 0)
	count := int(0)
	var cmd_id, msg_id string

	for i := 0; i < qinfo.Messages; i++ {
		msg, ok, err := ch.Get(qname, false)
		if !ok || err != nil {
			return count, err
		}

		if val, found := msg.Headers["uuid"]; found {
			cmd_id, _ = val.(string)
		} else {
			cmd_id = ""
		}

		if val, found := msg.Headers["dbid"]; found {
			msg_id, _ = val.(string)
		} else {
			msg_id = msg.CorrelationId
		}

		if msg_id == id || cmd_id == id {
			// ACK the message to remove it from the queue
			msg.Ack(false)
			count++
		} else {
			msgs = append(msgs, msg)
		}

	}

	// Requeue messages
	for _, msg := range msgs {
		msg.Reject(true)
	}

	return count, nil
}
