/*
Copyright © 2019 Michael Kenney <mikek@apl.uw.edu>

*/
package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/streadway/amqp"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var (
	cfgFile string
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "mlf2msg",
	Short: "MLF2 message management",
}

func Execute(version string) {
	rootCmd.Version = version
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	home, _ := homedir.Dir()
	defcfg := filepath.Join(home, ".config", "mlf2msg.toml")

	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "",
		fmt.Sprintf("config file (default %s)", defcfg))
	rootCmd.PersistentFlags().String("addr", "localhost:5672", "AMQP server address")
	viper.BindPFlag("addr", rootCmd.PersistentFlags().Lookup("addr"))
	rootCmd.PersistentFlags().String("user", "guest", "AMQP username")
	viper.BindPFlag("user", rootCmd.PersistentFlags().Lookup("user"))
	rootCmd.PersistentFlags().String("pass", "", "AMQP user password")
	viper.BindPFlag("pass", rootCmd.PersistentFlags().Lookup("pass"))
	rootCmd.PersistentFlags().String("vhost", "mlf2", "AMQP virtual host")
	viper.BindPFlag("vhost", rootCmd.PersistentFlags().Lookup("vhost"))
	rootCmd.PersistentFlags().String("exchange", "commands", "AMQP exchange name")
	viper.BindPFlag("exchange", rootCmd.PersistentFlags().Lookup("exchange"))

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in $HOME/.config directory with name "mlf2msg" (without extension).
		viper.AddConfigPath(filepath.Join(home, ".config"))
		viper.SetConfigName("mlf2msg")
	}

	viper.SetEnvPrefix("mlf2msg")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

func mqSetup(floatid int) (*amqp.Channel, error) {
	uri := fmt.Sprintf("amqp://%s:%s@%s/%s",
		viper.GetString("user"),
		viper.GetString("pass"),
		viper.GetString("addr"),
		viper.GetString("vhost"))

	conn, err := amqp.Dial(uri)
	if err != nil {
		return nil, errors.Wrap(err, "connect")
	}

	channel, err := conn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "open channel")
	}

	qname := fmt.Sprintf("float-%d.commands", floatid)

	if err = channel.ExchangeDeclare(
		viper.GetString("exchange"), // name of the exchange
		"topic",                     // type
		true,                        // durable
		false,                       // delete when complete
		false,                       // internal
		false,                       // noWait
		nil,                         // arguments
	); err != nil {
		return nil, errors.Wrap(err, "exchange declare")
	}

	_, err = channel.QueueDeclare(
		qname, // name of the queue
		true,  // durable
		false, // delete when usused
		false, // exclusive
		false, // noWait
		nil,   // arguments
	)
	if err != nil {
		return nil, errors.Wrap(err, "queue declare")
	}

	if err = channel.QueueBind(
		qname, // name of the queue
		qname, // bindingKey
		viper.GetString("exchange"), // sourceExchange
		false, // noWait
		nil,   // arguments
	); err != nil {
		return nil, errors.Wrap(err, "queue bind")
	}

	return channel, nil
}
