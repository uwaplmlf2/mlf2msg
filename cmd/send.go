/*
Copyright © 2019 Michael Kenney <mikek@apl.uw.edu>

*/
package cmd

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// sendCmd represents the send command
var sendCmd = &cobra.Command{
	Use:          "send floatid [cmdfile]",
	Short:        "Send a message to a float",
	Args:         cobra.MinimumNArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		sender := viper.GetString("sender")
		floatid, err := strconv.Atoi(args[0])
		if err != nil {
			errors.Wrap(err, "read float ID")
		}
		ch, err := mqSetup(floatid)
		if err != nil {
			return err
		}
		defer ch.Close()

		var infile io.Reader
		if len(args) > 1 {
			f, err := os.Open(args[1])
			if err != nil {
				return errors.Wrap(err, "open message file")
			}
			defer f.Close()
			infile = f
		} else {
			infile = os.Stdin
		}

		cmds, err := readMessage(infile)
		if err != nil {
			return err
		}
		id, err := publishCommands(ch,
			viper.GetString("exchange"),
			floatid,
			cmds,
			sender,
			viper.GetString("comment"))
		fmt.Println(id)
		return err
	},
}

func init() {
	rootCmd.AddCommand(sendCmd)

	sendCmd.Flags().String("sender", "", "Email address for message reply (required)")
	sendCmd.MarkFlagRequired("sender")
	viper.BindPFlag("sender", sendCmd.Flags().Lookup("sender"))
	sendCmd.Flags().String("comment", "", "Message comment for reference")
	viper.BindPFlag("comment", sendCmd.Flags().Lookup("comment"))
}

func parseCommand(input string) string {
	if strings.HasPrefix(input, "!") {
		return input[1:]
	}
	fields := strings.Split(input, "=")
	if len(fields) == 1 {
		return "get " + fields[0]
	}
	return fmt.Sprintf("set %s %s", strings.Trim(fields[0], "\t "),
		strings.Trim(fields[1], "\t "))
}

func readMessage(r io.Reader) ([]string, error) {
	contents := make([]string, 0)
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		contents = append(contents, parseCommand(scanner.Text()))
	}
	return contents, scanner.Err()
}
