/*
Copyright © 2019 Michael Kenney <mikek@apl.uw.edu>

*/
package cmd

import (
	"os"
	"strconv"
	"text/template"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const TEMPLATE = "{{.Sender}}\t{{.MsgId}}\t{{.Command}}\t{{.CommandId}}"

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:          "list floatid",
	Short:        "List all queued commands for a float",
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		tmpl, err := template.New("list").Parse(viper.GetString("format"))
		if err != nil {
			errors.Wrap(err, "parsing template")
		}
		floatid, err := strconv.Atoi(args[0])
		if err != nil {
			errors.Wrap(err, "read float ID")
		}
		ch, err := mqSetup(floatid)
		if err != nil {
			return err
		}
		defer ch.Close()

		listing, err := getAll(ch, floatid)
		if err != nil {
			return err
		}
		for _, item := range listing {
			err = tmpl.Execute(os.Stdout, item)
			if err != nil {
				return err
			}
			os.Stdout.Write([]byte("\n"))
		}
		return nil
	},
}

func init() {
	rootCmd.AddCommand(listCmd)
	listCmd.Flags().StringP("format", "f", TEMPLATE, "Format of queue listing")
	viper.BindPFlag("format", listCmd.Flags().Lookup("format"))
}
