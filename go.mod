module bitbucket.org/uwaplmlf2/mlf2msg

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/rs/xid v1.2.1
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/streadway/amqp v0.0.0-20190312223743-14f78b41ce6d
)

go 1.13
