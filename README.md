# MLF2 Message Management

This program allows the user to interact directly with the command queue
for an MLF2 float.

## Background

Command messages to and MLF2 float have traditionally been sent through a
web-page form interface. This interface stores each message in a database
and a server-side process splits each message into separate commands and
sends each command to an [AMQP Message
Queue](https://www.rabbitmq.com/tutorials/amqp-concepts.html) for transfer
to the float when it next calls-in. This program allows the user to bypass
the web form and database.

## Installation

See the [Installation Guide](https://bitbucket.org/uwaplmlf2/mlf2msg/wiki/Home).

## Usage

Use the `--help` option to view a command synopsis:

``` shellsession
$ mlf2msg --help
MLF2 message management

Usage:
  mlf2msg [command]

Available Commands:
  del         Remove one or more commands from the queue
  help        Help about any command
  list        List all queued commands for a float
  send        Send a message to a float

Flags:
      --addr string       AMQP server address (default "localhost:5672")
      --config string     config file (default is $HOME/.config/mlf2msg.toml)
      --exchange string   AMQP exchange name (default "commands")
  -h, --help              help for mlf2msg
      --pass string       AMQP user password
      --user string       AMQP username (default "guest")
      --version           version for mlf2msg
      --vhost string      AMQP virtual host (default "mlf2")

Use "mlf2msg [command] --help" for more information about a command.
```

### Setting command options

Values for the options can be specified on the command-line, in a [TOML
format](https://github.com/toml-lang/toml) configuration file, or with
environment variables. Configuration file variables have the same name as
the command line flags without the leading `--`. Environment variables are
in all caps and are prefixed with `MLF2MSG_`

If you store the password in a file, insure that the file can only be read
from your account.

#### Example configuration file

``` toml
addr = "128.97.97.213:5672"
user = "myusername"
pass = "mypassword"
```

#### Example environment variable setting

``` shell
export MLF2MSG_ADDR="128.97.97.213:5672"
export MLF2MSG_USER="myusername"
export MLF2MSG_PASS="mypasword"
```

### Send a message

Compose your message in an ASCII text file with one command on each line
(similar to the web-page form). Lines may be terminated with a line-feed
or a carriage-return/line-feed sequence.

Upload your message using the `send` subcommand which has one required
argument (the float-ID) and one optional argument (command-file name). The
commmand-file contents are read from standard input if no file name is supplied.

``` shellsession
$ mlf2msg send --help
Send a message to a float

Usage:
  mlf2msg send floatid [cmdfile] [flags]

Flags:
      --comment string   Message comment for reference
  -h, --help             help for send
      --sender string    Email address for message reply (required)

Global Flags:
      --addr string       AMQP server address (default "localhost:5672")
      --config string     config file (default is $HOME/.config/mlf2msg.toml)
      --exchange string   AMQP exchange name (default "commands")
      --pass string       AMQP user password
      --user string       AMQP username (default "guest")
      --vhost string      AMQP virtual host (default "mlf2")
```

In the example below, we compose a file and send it to float-0.

``` shellsession
$ cat<<EOF > floatcmds
> comm:wait_timeout=7200
> !wait
> EOF
$ mlf2msg --config config.toml send --sender mikek@apl.uw.edu 0 floatcmds
Using config file: config.toml
bmj1k5th5s5mkhrot5og
```

The `send` operation returns a unique-id (UUID) for the command file. We can then
use the `list` subcommand to view the pending messages for float-0.

``` shellsession
$ mlf2msg --config config.toml list 0
Using config file: config.toml
mikek@apl.uw.edu	bmj1k5th5s5mkhrot5og	set comm:wait_timeout 7200	bmj1k5th5s5mkhrot5p0
mikek@apl.uw.edu	bmj1k5th5s5mkhrot5og	wait	bmj1k5th5s5mkhrot5pg
```

A separate message is created from each line of the command file. The
listing output contains four columns; sender email address, UUID for the
command file, message contents, and UUID for the message. The UUIDs are
used when you want to delete a message or all message from a single
command file.

### Delete messages

Messages can be deleted with the `del` subcommand. This subcommand has two
required arguments, the float-id and the message UUID (or command-file UUID)

``` shellsession
$ mlf2msg del --help
Remove one or more commands from the queue

Usage:
  mlf2msg del floatid id [flags]

Flags:
  -h, --help   help for del

Global Flags:
      --addr string       AMQP server address (default "localhost:5672")
      --config string     config file (default is $HOME/.config/mlf2msg.toml)
      --exchange string   AMQP exchange name (default "commands")
      --pass string       AMQP user password
      --user string       AMQP username (default "guest")
      --vhost string      AMQP virtual host (default "mlf2")
```

In this example we delete the first message from the listing

``` shellsession
$ mlf2msg --config config.toml del 0 bmj1k5th5s5mkhrot5p0
Using config file: config.toml
1 commands removed
```

Confirm by listing the queue.

``` shellsession
$ mlf2msg --config config.toml list 0
Using config file: config.toml
mikek@apl.uw.edu	bmj1k5th5s5mkhrot5og	wait	bmj1k5th5s5mkhrot5pg
```
